### Train on Zeuthen Cluster

If you are working out of the DESY Zeuthen servers, `warp.zeuthen.desy.de`, you can train using the batch system via `qsub` and GPU support using the `umami-config-tags/institutes/zeuthen/train_job.sh` script:

```bash
qsub train_job.sh -c training-config.yml -e 200
```

The job will output a log to the current working directory and copy the results to the current working directory when it's done. The options for the job (time, memory, space, etc.) can be changed in `umami-config-tags/institutes/zeuthen/train_job.sh`.
